const express = require('express')
const app = express()
const path = require('path');
const router = express.Router();

router.get('/',function(req,res){
    res.sendFile(path.join(__dirname+'/views/index.html'));
});

router.get('/kanette',function(req,res){
    res.sendFile(path.join(__dirname+'/views/kanette.html'));
});

app.use('/', router)

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
})