'use strict';

const electron = require('electron');
require('./app')

const app = electron.app;

const BrowserWindow = electron.BrowserWindow;

let mainWindow = null;

app.on('window-all-closed', function() {

    app.quit();

});

app.on('ready', function() {

    mainWindow = new BrowserWindow({width: 800, height: 600});

    mainWindow.loadURL('http://localhost:3000');

    mainWindow.on('closed', function() {

        mainWindow = null;

    });

});